var path = require("path")
var HtmlWebpackPlugin = require("html-webpack-plugin")
var ExtractTextPlugin = require("extract-text-webpack-plugin")

var config = {
    context: __dirname,
    entry: {
        mainjs: ["babel-polyfill", "./src/js/index"] // entry point of our app. src/js/index.js should require other js modules and dependencies it needs
    },
    output: {
        path: path.resolve("./build/"),
        filename: "[name]-[hash].js",
        publicPath: "/",
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader",
                query: {
                    presets: ["react", "es2015", "stage-0"],
                    plugins: [
                        "transform-decorators-legacy",
                        "transform-react-remove-prop-types",
                        "transform-react-inline-elements",
                    ],
                }
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader"
                })
            },
            {
                test: /\.ttf$/,
                loader: "file-loader"
            },
            {
                test: /\.otf$/,
                loader: "file-loader"
            },
            {
                test: /\.png$/,
                loader: "url-loader?limit=8192&mimetype=image/png"
            },
            {
                test: /\.jpg$/,
                loader: "url-loader?limit=8192&mimetype=image/jpg"
            },
            {
                test: /\.ico/,
                loader: "url-loader"
            },
            {
                test: /\.svg$/,
                loader: "svg-inline-loader"
            }
        ],
    },
    resolve: {
        modules: ["node_modules"],
        extensions: [".js", ".jsx"]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "[name]-[hash].css",
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            hash: true,
            title: "ScatterPlot",
            inject: false,
            template: "index.ejs"
        })
    ]
}

module.exports = config
