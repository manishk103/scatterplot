Steps to get started with scatterplot component

1. Run the command to install all the dependencies -> 
    "npm install"

2. Run the command to start the app ->  
    npm start

3. Open url localhost:8080 in any browser
