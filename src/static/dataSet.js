export default [
    {
        name: "Data Set 1",
        value: [
            {
                "start_time": "2017-12-29T04:56:12Z",
                "status": "pass",
                "duration": 126, // in seconds
            },
            {
                "start_time": "2017-11-30T03:22:12Z",
                "status": "error",
                "duration": 205,
            },
            {
                "start_time": "2017-12-04T02:24:12Z",
                "status": "fail",
                "duration": 20,
            },
            {
                "start_time": "2017-12-08T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2017-12-12T05:24:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2017-12-20T06:24:12Z",
                "status": "error",
                "duration": 90,
            },
            {
                "start_time": "2017-11-28T14:12:12Z",
                "status": "pass",
                "duration": 200,
            },
            {
                "start_time": "2018-01-13T14:12:12Z",
                "status": "pass",
                "duration": 300,
            },
            {
                "start_time": "2017-10-08T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2017-10-14T05:24:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2017-10-20T06:24:12Z",
                "status": "error",
                "duration": 60,
            },
            {
                "start_time": "2017-10-30T06:24:12Z",
                "status": "pass",
                "duration": 160,
            },
            {
                "start_time": "2017-11-04T06:24:12Z",
                "status": "pass",
                "duration": 260,
            },
            {
                "start_time": "2017-11-08T14:12:12Z",
                "status": "error",
                "duration": 70,
            },
            {
                "start_time": "2017-11-24T06:24:12Z",
                "status": "fail",
                "duration": 60,
            },
            {
                "start_time": "2017-12-20T14:12:12Z",
                "status": "pass",
                "duration": 170,
            },
            {
                "start_time": "2018-01-01T14:12:12Z",
                "status": "pass",
                "duration": 240,
            },
            {
                "start_time": "2017-10-20T06:24:12Z",
                "status": "pass",
                "duration": 260,
            },
            {
                "start_time": "2017-11-17T14:12:12Z",
                "status": "pass",
                "duration": 40,
            },
            {
                "start_time": "2018-01-07T14:12:12Z",
                "status": "pass",
                "duration": 140,
            }
        ]
    }, 
    {
        name: "Data Set 2",
        value: [
            {
                "start_time": "2018-02-02T04:56:12Z",
                "status": "pass",
                "duration": 226,
            },
            {
                "start_time": "2018-02-16T03:22:12Z",
                "status": "error",
                "duration": 205,
            },
            {
                "start_time": "2018-03-04T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2018-03-08T02:24:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2018-04-12T05:24:12Z",
                "status": "error",
                "duration": 100,
            },
            {
                "start_time": "2018-04-28T06:24:12Z",
                "status": "pass",
                "duration": 90,
            },
            {
                "start_time": "2018-06-05T14:12:12Z",
                "status": "error",
                "duration": 140,
            },
            {
                "start_time": "2018-01-13T14:12:12Z",
                "status": "pass",
                "duration": 280,
            },
            {
                "start_time": "2018-07-17T14:12:12Z",
                "status": "error",
                "duration": 190,
            },
            {
                "start_time": "2018-07-15T14:12:12Z",
                "status": "pass",
                "duration": 30,
            },
            {
                "start_time": "2018-03-18T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2018-04-16T05:24:12Z",
                "status": "error",
                "duration": 180,
            },
            {
                "start_time": "2018-04-20T06:24:12Z",
                "status": "pass",
                "duration": 160,
            },
            {
                "start_time": "2018-05-01T14:12:12Z",
                "status": "error",
                "duration": 120,
            },
            {
                "start_time": "2018-05-19T14:12:12Z",
                "status": "fail",
                "duration": 50,
            },
            {
                "start_time": "2018-04-10T14:12:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2018-07-10T14:12:12Z",
                "status": "pass",
                "duration": 300,
            }
        ]
    },
    {
        name: "Data Set 3",
        value: [
            {
                "start_time": "2018-01-12T04:56:12Z",
                "status": "pass",
                "duration": 50,
            },
            {
                "start_time": "2018-01-16T03:22:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2018-02-04T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2018-02-08T02:24:12Z",
                "status": "pass",
                "duration": 200,
            },
            {
                "start_time": "2018-02-18T05:24:12Z",
                "status": "error",
                "duration": 40,
            },
            {
                "start_time": "2018-03-01T06:24:12Z",
                "status": "pass",
                "duration": 190,
            },
            {
                "start_time": "2018-03-05T14:12:12Z",
                "status": "error",
                "duration": 140,
            },
            {
                "start_time": "2018-03-13T14:12:12Z",
                "status": "pass",
                "duration": 180,
            },
            {
                "start_time": "2018-04-17T14:12:12Z",
                "status": "error",
                "duration": 100,
            },
            {
                "start_time": "2018-04-25T14:12:12Z",
                "status": "pass",
                "duration": 30,
            },
            {
                "start_time": "2018-05-18T02:24:12Z",
                "status": "fail",
                "duration": 120,
            },
            {
                "start_time": "2018-04-16T05:24:12Z",
                "status": "error",
                "duration": 180,
            },
            {
                "start_time": "2018-04-20T06:24:12Z",
                "status": "pass",
                "duration": 160,
            },
            {
                "start_time": "2018-05-01T14:12:12Z",
                "status": "error",
                "duration": 120,
            },
            {
                "start_time": "2018-06-19T14:12:12Z",
                "status": "fail",
                "duration": 50,
            },
            {
                "start_time": "2018-07-01T14:12:12Z",
                "status": "pass",
                "duration": 100,
            },
            {
                "start_time": "2018-07-10T14:12:12Z",
                "status": "pass",
                "duration": 300,
            }
        ]
    }
]