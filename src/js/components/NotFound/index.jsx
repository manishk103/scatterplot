import React, {Component} from "react"
import { Link } from "react-router-dom"

export default class NotFound extends Component {
    render() {
        return (
            <div className="row">
                <div className="col s12 m8 offset-m2">
                    <h3 className="blue-grey-text lighten-3 center-align">Page does not exist.</h3>
                    <p className="center-align">
                        <Link to="/" className="blue-text center-align">Return to homepage</Link>
                    </p>
                </div>
            </div>
        )
    }
}
