import React, {
    Component
} from "react"
import PropTypes from "prop-types"

export default class ScatterPlot extends Component {
    render() {
        const {children} = this.props
        return (
            <div className="fluid-container">
                {children}
            </div>
        )
    }
}

ScatterPlot.propTypes = {
    children: PropTypes.node
}
