import React, {
    Component
} from "react"
import PropTypes from "prop-types"
import moment from "moment"
import DatePicker from "react-datepicker"

import Point from "./Point"
import style from "./Home.css"

import dataSet from "../../../static/dataSet"

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            plotPoints: props.plotPoints || [],
            filterStartDate: "",
            filterEndDate: "",
            currentDataSet: dataSet[0].name
        }
    }
    componentDidMount = () => {
        const { plotPoints } = this.state
        const allDates = plotPoints.map(d => moment(d.start_time))
        const maxDate = moment.max(allDates)
        const minDate = moment.min(allDates)
        this.setState({
            filterStartDate: minDate,
            filterEndDate: maxDate,
        })
    }
    componentWillReceiveProps = (newProps) =>  {

    }
    insertText = (x, y, text) => {
        return <text x={x} y={y} fill="" className={style.axisLabel}>{text}</text>
    }
    drawXaxisTick = (x1, x2) => {
        const { height, margin: {bottom} } = this.props
        return <line x1={x1} y1={height - bottom} x2={x1} y2={(height - bottom) + 5} stroke="black" />
    }
    drawYaxisTick = (y1, y2) => {
        const { margin: {left} } = this.props
        return <line x1={left} y1={y1} x2={left-5} y2={y1} stroke="black" />
    }
    drawYaxisParallelLine = (y1, y2) => {
        const { width, margin: {left, right} } = this.props
        return <line x1={left} y1={y1} x2={width-right} y2={y1} className={style.yAxisLine} />
    }
    renderTime = () => {
        const { height, width, margin: { left, right, bottom }} = this.props
        const { plotPoints } = this.state

        const avilableWidth = width - (left + right)
        
        const allDates = plotPoints.map(d => moment(d.start_time))
        const maxDate = moment.max(allDates)
        const minDate = moment.min(allDates)
        const daysDiff = maxDate.diff(minDate, "days")
        
        let totalDatesToDisplay = 2
        let dateFormatToDisplay = "DD MMM hh:mm"

        if (daysDiff <= 2) {
            totalDatesToDisplay = 2
            dateFormatToDisplay = "DD MMM YY hh:mm"
        } else if (daysDiff > 2 && daysDiff <= 30) {
            totalDatesToDisplay = 6
            dateFormatToDisplay = "DD MMM"
        } else if (daysDiff > 30 && daysDiff <= 365) {
            totalDatesToDisplay = 6
            dateFormatToDisplay = "DD MMM YY"
        } else if (daysDiff > 365) {
            totalDatesToDisplay = 6
            dateFormatToDisplay = "MMM YY"
        }

        const dateXoffset = avilableWidth / totalDatesToDisplay
        const datesToShow = []
        
        for (let i = 0; i < totalDatesToDisplay+1; i++) {
            const x1 = left + (dateXoffset * i)
            let date = ""
            if (i == 0) {
                date = minDate.format(dateFormatToDisplay)
            } else if (i === totalDatesToDisplay) {
                date = maxDate.format(dateFormatToDisplay)
            } else {
                date = moment(minDate).add((daysDiff / totalDatesToDisplay) * i, "days").format(dateFormatToDisplay)
            }
            const data = <g>
                {this.drawXaxisTick(x1)}
                {this.insertText(x1 - 20, (height - bottom) + 15, date)}
            </g>
            datesToShow.push(data)
        }
        return datesToShow
    }
    renderDuration = () => {
        const { height, margin: {top, bottom }} = this.props
        const { plotPoints } = this.state

        const avilableHeight = height - (top + bottom)
        
        const allDurations = plotPoints.map(d => d.duration)
        const maxDuration = Math.max(...allDurations)

        let totalDurationsToDisplay = 6
        
        // if (durationDiffInMin > 2 && durationDiffInMin <= 60) {
        //     totalDurationsToDisplay = 6
        // }
        const dateYoffset = avilableHeight / totalDurationsToDisplay
        
        const durationsToShow = []
        const interval = maxDuration / totalDurationsToDisplay
        for (let i = 0; i <= totalDurationsToDisplay; i++) {
            const y1 = (height - bottom) - (dateYoffset * i)
            let duration = interval * i // this is in sec
            const data = <g>
                { this.drawYaxisTick(y1)}
                { i !== 0 && this.drawYaxisParallelLine(y1)}
                { this.insertText(0, y1+4, (duration / 60).toFixed(1)+" min")}
            </g>
            durationsToShow.push(data)
        }
        return durationsToShow
    }
    renderPoints = () => {
        const { height, width, statusColor , margin: { left,right,top,bottom }} = this.props
        const { plotPoints } = this.state

        const avilableHeight = height - (top + bottom)
        const avilableWidth = width - (left + right)

        const allDurations = plotPoints.map(d => d.duration)
        const maxDuration = Math.max(...allDurations)
        
        const allDates = plotPoints.map(d => moment(d.start_time).valueOf())
        const maxDate = Math.max(...allDates)
        const minDate = Math.min(...allDates)
        const maxMinDiff = maxDate - minDate

        return plotPoints.map((d, index) => {
            const x = left + (avilableWidth / (maxMinDiff / (moment(d.start_time).valueOf() - minDate)))
            const y = (height - bottom) - (avilableHeight / maxDuration * d.duration)
            const color = statusColor[d.status]
            return <Point x={x} y={y} r="6" fill={color} />
        })
    }
    drawChart = () => {
        const { height, width, margin: {left,right,top,bottom} } = this.props
        
        return (<svg 
            className="Scatterplot"
            width={width}
            height={height}>

            {/* x-axis line */}
            <line x1={left} y1={height - bottom} x2={width - right} y2={height - bottom} stroke="black" />
            {/* y-axis line */}
            <line x1={left} y1={height - bottom} x2={left} y2={top} stroke="black" />

            {this.renderPoints()}
            
            {this.renderTime()}

            {this.renderDuration()}
            
        </svg>)
    }
    handleFromDateChange = (date) => {
        const { currentDataSet, filterEndDate } = this.state
        const plotPoints = dataSet.find(d => d.name === currentDataSet).value
        const filteredPlotPoints = plotPoints.filter(d => moment(d.start_time).isBetween(date, filterEndDate))
        this.setState({ 
            filterStartDate: date,
            plotPoints: filteredPlotPoints 
        })
    }
    handleToDateChange = (date) => {
        const { currentDataSet, filterStartDate } = this.state
        const plotPoints = dataSet.find(d => d.name === currentDataSet).value
        const filteredPlotPoints = plotPoints.filter(d => moment(d.start_time).isBetween(filterStartDate, date))
        this.setState({ 
            filterEndDate: date,
            plotPoints: filteredPlotPoints 
        })
    }
    resetFilterDate = () => {
        const { currentDataSet } = this.state
        const plotPoints = dataSet.find(d => d.name === currentDataSet).value
        const allDates = plotPoints.map(d => moment(d.start_time))
        const maxDate = moment.max(allDates)
        const minDate = moment.min(allDates)
        this.setState({
            filterEndDate: maxDate,
            filterStartDate: minDate,
            plotPoints
        })
    }
    renderFilterDateRange = () => {
        const { currentDataSet, filterStartDate, filterEndDate } = this.state
        const plotPoints = dataSet.find(d => d.name === currentDataSet).value
        const allDates = plotPoints.map(d => moment(d.start_time))
        const maxDate = moment.max(allDates)
        const minDate = moment.min(allDates)
        return (<div className="row">
            <div className="col s12 m5">
                <span>From Date</span>
                <DatePicker
                    selected={filterStartDate}
                    minDate={moment(minDate)}
                    maxDate={moment(maxDate)}
                    onChange={this.handleFromDateChange}
                    dateFormat="DD MMM YYYY"
                />
            </div>
            <div className="col s12 m5">
                <span>To Date</span>
                <DatePicker
                    selected={filterEndDate || moment(maxDate)}
                    minDate={filterStartDate || moment(minDate)}
                    maxDate={moment(maxDate)}
                    onChange={this.handleToDateChange}
                    dateFormat="DD MMM YYYY"
                />
            </div>
            <div className="col s12 m12">
                <a onClick={this.resetFilterDate} className="btn blue-grey white-text">Reset Date</a>
            </div>
        </div>)
    }
    changeDataSet = (e) => {
        const newDataSet = dataSet.find(d => d.name === e.target.value).value
        const allDates = newDataSet.map(d => moment(d.start_time))
        const maxDate = moment.max(allDates)
        const minDate = moment.min(allDates)
        this.setState({
            currentDataSet: e.target.value,
            plotPoints: newDataSet,
            filterStartDate: minDate,
            filterEndDate: maxDate,
        })
    }
    renderChooseDataSet = () => {
        const { currentDataSet } = this.state
        return (<div className="row">
            <div className="col s12 m12">
                <span>Select Data Set</span>
                <select className="browser-default" onChange={this.changeDataSet} defaultValue={currentDataSet}>
                    <option value="" disabled>Choose data set</option>
                    {dataSet.map(d => <option key={d.name} value={d.name}>{d.name}</option>)}
                </select>
            </div>
        </div>)
    }
    render() {
        return (
            <div className="row">
                <div className="col s12 m12 grey lighten-3">
                    <h4 className="center-align blue-text text-darken-1">Scatterplot Chart</h4>
                </div>
                <div className="col s12 m4">
                    <h5 className="blue-grey-text lighten-2 center-align">Set Date Range</h5>
                    {this.renderChooseDataSet()}
                    {this.renderFilterDateRange()}
                </div>
                <div className="col s12 m8 center-align">
                    <div className={style.colorLable}>
                        <div><div className={style.circle + " green"}></div> pass</div>
                        <div><div className={style.circle + " red"}></div> fail</div>
                        <div><div className={style.circle + " orange"}></div> error</div>
                    </div>
                    {this.drawChart()}      
                </div>
                <div className="col s12 m8 offset-m2 center-align">
                    <div className="row">
                        <div className="col s12 m12">
                            <h4 className="blue-grey-text lighten-2">Ploting Points</h4>
                            <table className="striped">
                                <thead>
                                    <tr>
                                        <th>Time</th>
                                        <th>Status</th>
                                        <th>Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.plotPoints.map(d => <tr>
                                        <td>{moment(d.start_time).format("DD MMM YYY hh:mm")}</td>
                                        <td className={d.status === "pass" ? "green-text" 
                                            : d.status === "fail" ? "red-text" : "orange-text" }>{d.status}</td>
                                        <td>{d.duration}</td>
                                    </tr>)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Home.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    margin: PropTypes.object,
    plotPoints: PropTypes.array,
    statusColor: PropTypes.object,
}

Home.defaultProps = {
    height: 350,
    width: 600,
    statusColor: {
        "fail": "red",
        "pass": "green",
        "error": "orange"
    },
    margin: {
        top: 20,
        bottom: 20,
        left: 50,
        right: 20,
    },
    plotPoints: dataSet[0].value
}