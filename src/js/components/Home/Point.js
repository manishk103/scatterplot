import React from "react"
import PropTypes from "prop-types"

import style from "./Home.css"

class Point extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: false
        }
    }
    componentWillReceiveProps = (newProps) => {
        const { x,y } = this.props
        if (newProps.x !== x || newProps.y !== y) {
            this.setState({ selected: false })
        }
    }
    handlePointClick = () => {
        this.setState({ 
            selected: !this.state.selected
        })
    }
    render() {
        const { x, y, r, fill, stroke, strokeWidth } = this.props
        const { selected } = this.state
        return (<g className="Point">
            <circle
                cx={x}
                cy={y}
                r={selected && 8 || r}
                fill={fill}
                className={selected && style.selectedPoint || style.normalPoint}
                stroke={stroke}
                strokeWidth={strokeWidth} 
                onClick={this.handlePointClick}>
            </circle>
        </g>)
    }
}

Point.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    r: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number
}

Point.defaultProps = {
    x: 10,
    y: 10,
    r: 3,
    fill: "red",
    stroke: "none",
    strokeWidth: 0,
}

export default Point