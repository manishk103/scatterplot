import { fetchActionCreator } from "redux-fetch-helpers"
import { InvalidAuthDetails, APIError } from "../errors"

const apiBaseUrl = ""

const API_ENDPOINT = {

}

const fetchData = (action, url, requestMethod, requestBody, extra=null) => {
    return fetchActionCreator({
        url,
        fetchOptions: Object.assign({
            method: requestMethod,
            headers: new Headers({
                "Accept": "application/json",
                "Content-Type": "application/json",
            }),
        }, requestMethod === "POST" ? {
            body: JSON.stringify(requestBody)
        } : {}),
        actionType: action,
        responseConfig: {
            200: (resp => extra ? Object.assign({},extra,JSON.parse(resp)) : JSON.parse(resp)),
            400: (payload => new InvalidAuthDetails(payload)),
            other: (payload => new APIError(payload)),
        }
    })
}

const api = {
    
}

export default api
