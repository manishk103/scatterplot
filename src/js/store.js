import thunk from "redux-thunk"
import reducers from "./reducers"
import createLogger from "redux-logger"
import { routerMiddleware } from "react-router-redux"
import { 
    createStore, 
    applyMiddleware, 
    compose 
} from "redux"

import transition from "./middlewares/transition"

export const configureStore = (initialState, history) => {
    const middlewares = [
        thunk,
        transition,
        routerMiddleware(history)
    ]
    middlewares.push(createLogger())
    return createStore(
        reducers,
        initialState,
        compose(
            applyMiddleware.apply(null, middlewares),
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    )
}
