/* global */
import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { browserHistory } from "react-router"
import { syncHistoryWithStore } from "react-router-redux"
import { BrowserRouter as Router, Switch, Route} from "react-router-dom"

import { configureStore } from "./store"

import Home from "./components/Home"
import NotFound from "./components/NotFound"
import Scatterplot from "./components/ScatterPlot"

import "react-datepicker/dist/react-datepicker-cssmodules.css"

document.addEventListener("DOMContentLoaded", () => {

    const initialState = {
        // reducers initial state
    }
    const store = configureStore(initialState, browserHistory)
    const history = syncHistoryWithStore(browserHistory, store)
    
    const appContainer = document.getElementById("app-container")

    ReactDOM.render(
        (<Provider store={store}>
            <Router history={history}>
                <Scatterplot>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="*" component={NotFound}/>
                    </Switch>
                </Scatterplot>
            </Router>
        </Provider>),
        appContainer
    )
})
